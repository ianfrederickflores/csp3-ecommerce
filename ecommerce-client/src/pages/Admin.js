import {Container} from 'react-bootstrap';
import {Row, Col} from "react-bootstrap";
import React, { Fragment, useEffect, useState, useContext } from 'react';
import ProductCard from '../components/ProductCard';

export default function Admin(){

	const [allProducts, setAllProducts] = useState([]);

	useEffect(() => {
		fetch('http://localhost:4000/products/all')
		.then(res => res.json())
		.then(data => {

			setAllProducts(data.map(product => {
					return(
						<ProductCard key={product.id} productProp={product} />
					)
				})
			);
			
		})
	}, [allProducts])

	return(
		<Container>
			<Row className='mt-3 mb-3'>
				<h1>
					Admin Dashboard
				</h1>
			</Row>

			<Row className='mt-3 mb-3'>
				<Col>
					<button type="button" class="btn btn-primary">Add New Product</button>
				</Col>
				<Col>
					
				</Col>				
			</Row>

			<Row className='mt-3 mb-3'>
				<table class="table table-bordered table-striped table-dark">
				  <thead>
				    <tr>
				      <th scope="col">Name</th>
				      <th scope="col">Description</th>
				      <th scope="col">Price</th>
				      <th scope="col">Availability</th>
				      <th scope="col">Actions</th>
				    </tr>
				  </thead>
				  <tbody>
				    <tr>
				      <td></td>
				      <td></td>
				      <td></td>
				      <td></td>
				      <td>
				      	<button type="button" class="btn btn-primary">Update</button>
				      	<button type="button" class="btn btn-danger">Disable</button>
				      </td>		      
				    </tr>		    
				  </tbody>
				</table>
			</Row>			
		</Container>
	)
}