import React from 'react';
import { useState, useEffect } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';

import ProductCard from '../components/ProductCard';



export default function ProductView(props) {

	const productId = props.match.params.productId;
	const userId = localStorage.getItem('id');

	const [name, setName] = useState([]);
	const [description, setDescription] = useState([]);
	const [price, setPrice] = useState([]);

	useEffect(() => {
		fetch(`http://localhost:4000/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
				setName(data.name);
				setDescription(data.description);
				setPrice(data.price);
				}
			);
	}, [])


	const checkout = async () => {
        fetch(`http://localhost:4000/carts/${userId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                userId: userId,
				productId: productId,
				price: price,
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
   	    })
	}
	
	return(
		<Container className="mt-5">
				<Card>
					<Card.Body className="text-center">
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle>Description</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price</Card.Subtitle>
						<Card.Text> TG {price}</Card.Text>						
						<Button variant="primary" onClick={checkout} to={`/carts/${userId}`} block>Checkout</Button>
					</Card.Body>
				</Card>
		</Container>
	)
}
