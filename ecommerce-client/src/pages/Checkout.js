import {Container} from 'react-bootstrap';
import {Row, Col, Button} from "react-bootstrap";
import {useCounter} from "@chakra-ui/counter"


export default function Checkout(){
	const counter = useCounter({
    	max: 1000000,
    	min: 0,
    	step: 1,
  	})

	return(
		<Container>
			<Row>
				<h1 className='mt-3 mb-3'>
					Checkout
				</h1>
			</Row>

			<Row>
				<table class="table table-bordered table-striped table-dark">
				  <thead>
				    <tr>
				      <th scope="col">Name</th>
				      <th scope="col">Price</th>
				      <th scope="col">Quantity</th>
				      <th scope="col">Subtotal</th>
				      <th scope="col"></th>
				    </tr>
				  </thead>
				  <tbody>
				    <tr>
				      <td></td>
				      <td></td>
				      <td>
				      	  <button onClick={() => counter.increment()}>+</button>
				      	  <p>{counter.value}</p>
				      	  <button onClick={() => counter.decrement()}>-</button>
				      </td>
				      <td></td>
				      <td><button type="button" class="btn btn-danger">Remove</button></td>		      
				    </tr>		    
				  </tbody>
				</table>
			</Row>

			<Row>
				<table class="table table-bordered table-striped table-dark">
				  <thead>
				    <tr>
				      <th scope="col"><button type="button" class="btn btn-primary">Proceed</button></th>
				      <th scope="col">Total:</th>				      
				    </tr>
				  </thead>
				</table>
			</Row>			
		</Container>		
	)
}
