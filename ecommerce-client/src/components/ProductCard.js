import React, { Fragment, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function ProductCard({productProp}){

	const {_id, name, description, price} = productProp

	return(
		<Fragment xs={1}>
			<Row>
				<Col>
					<Card className="m-3">
			  			<Card.Body>
			    			<Card.Title>{name}</Card.Title>
			    			<Card.Text>{description}</Card.Text>
			    			<Card.Text>TG {price}</Card.Text>
			    			<Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>
			  			</Card.Body>
					</Card>
				</Col>
			</Row>
		</Fragment>
		)

};

ProductCard.propTypes = {
	product: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired,
	})

}
