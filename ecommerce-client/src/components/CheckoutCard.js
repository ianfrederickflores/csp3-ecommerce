import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function CartCard({cartProp}) {
	console.log({cartProp});
	const userId = localStorage.getItem('id');
	const {productId, quantity, price, subtotal} = cartProp;


	return (
		<Card className="m-3">
		  <Card.Body>
		    <Card.Title></Card.Title>
		    <Card.Text>{productId}</Card.Text>
		    <Card.Text>TG {price}</Card.Text>
		    <Card.Text>{quantity}</Card.Text>
		    <Card.Text>TG {subtotal}</Card.Text>
		    <Link className="btn btn-success" to={`/orders/${userId}`}>CheckOut</Link>
		  </Card.Body>
		</Card>
	)
}


CartCard.propTypes = {
	cart: PropTypes.shape({
		name: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired,
		quantity: PropTypes.number.isRequired,
	}) 
}
