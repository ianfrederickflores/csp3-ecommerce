const User = require("../models/Users");
const Product = require("../models/Products");
const Order = require("../models/Orders");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// User Registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		username: reqBody.username,
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNum: reqBody.mobileNum		
	})

	return newUser.save().then((user, err) => {
		if(err){
			return false;
		}

		else{
			return true;
		}
	})
};

// Check if the email already exists
module.exports.checkEmailExists = (reqBody) => {
	
	return User.find({email: reqBody.email}).then(result => {

		if(result.length > 0){
			return true;
		}
		else{
			return false;
		}
	})
}

// Get User Details
module.exports.getProfile = (req) => {
	return User.findById(req).then(result => {
		if(result == null) {return false} 
		else { 	result.password = ""; 
				return result;} })}

// New Admin
module.exports.registerAdmin = (reqBody) => {
	let newAdmin = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		username: reqBody.username,
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNum: reqBody.mobileNum,
		isAdmin: true		
	})

	return newAdmin.save().then((user, err) => {
		if(err){
			return false;
		}

		else{
			return true;
		}
	})
};

// User Authentication
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false;
		}

		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			}

			else{
				return false;
			}
			
		}
	})
};

// Setting another user as an admin
module.exports.updateAdmin = async (user, reqParams, reqBody) => {
	if(user.isAdmin){
		let updatedAdmin = {
			isAdmin: reqBody.isAdmin
		}

		return User.findByIdAndUpdate(reqParams.userId, updatedAdmin).then((user, err) => {
			if(err){
				return false;
			}
			else{
				return true;
			}
		})
	}

	else{
		return (`You have no access`);
	}
}

