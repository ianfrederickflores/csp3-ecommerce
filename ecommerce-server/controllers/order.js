const Order = require("../models/Orders");
const User = require("../models/Users");
const Product = require("../models/Products");

// Create New Order
module.exports.createOrder = async (user, customerId, data) => {

	let subtotal = 0;
	let totalPrice = 0;
	let productId = data.productId;
	let price = data.price;
	let quantity = data.quantity;
	subtotal += (price * quantity);
	totalPrice += subtotal;

	let newOrder = new Order({
		userId: userId,
		orders: {
			productId: productId,
			price: price,
			quantity: quantity,
			subtotal: subtotal },
		totalPrice: totalPrice })

	return newOrder.save().then((order, error) => {
		if(error) {return false}
		else {return order}})
	
	let isUserUpdated = await User.findById(userId)
	.then(user => {user.orders.push({orderId: orderId});
	return user.save().then((user, error) => {
		if(error) {return false}
		else {return true} })})

	if(isUserUpdated && isOrderUpdated) {return order} 
	else {return false}
}


// Retrieve authenticated user's order
module.exports.getMyOrders = async (user) => {
	return Order.find({customerId: user.id})
		.then(result => {
			if(result == null) {return false} 
			else {return result} })
}


// Retrieve all orders
module.exports.getAllOrders = async (user) => {
	if(user.isAdmin){
		return Order.find({}).then (result => {
		return result;
		})
	}

	else{
		return (`You have no access`);
	}
	
}
