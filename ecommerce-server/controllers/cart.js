const Cart = require('../models/Cart')
const auth = require('../auth')

// Retrieve All Items
module.exports.getAllInCart = (user) => {
	return Cart.find({}).then(result => {
		if(result == null) {return false} 
		else {return result} })
}

// Add to cart
module.exports.addToCart = (user, reqBody) => {

	let newCart = new Cart({
		customerId: reqBody.customerId,
		productId: reqBody.productId,
		price: reqBody.price,
		quantity: reqBody.quantity 
	});

	return newCart.save().then((product, error) => {
		if(error) {return false}
		else {return true} })
}

// Update Cart
module.exports.updateCart = async (user, reqParams, reqBody) => {
	
	let updatedCart = {
		productId: reqBody.productId,
		price: reqBody.price,
		quantity: reqBody.quantity 
	}

	return Cart.findByIdAndUpdate(reqParams.customerId, updatedCart)
	.then((product, error) => {
		if (error) {return false} 
		else {return product} })
}

// Remove from Cart
module.exports.archiveProduct = async (user, productId) => {
	return Cart.findByIdAndUpdate(cartId, {isActive: false})
	.then((product, error) => {
		if (error) {return false} 
		else {return product} })
}
