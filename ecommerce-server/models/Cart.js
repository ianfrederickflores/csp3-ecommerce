const mongoose = require('mongoose')

const cartSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "User Id is required"] 
	},

	ordersAdded: [{
		productId: {
			type: String
		},

		price: {
			type: Number
		},

		quantity: {
			type: Number
		},

		subtotal: {
			type: Number
		} 
	}],

	totalPrice: {type: Number},

	purchasedOn: {
		type: Date,
		default: Date.now 
	},

	isActive: {
		type: Boolean,
		default: true 
	},

	isCheckedOut: {
		type: Boolean,
		default: false 
	}
})

module.exports = mongoose.model('Cart', cartSchema);
