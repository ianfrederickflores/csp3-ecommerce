const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "User Id is required"]
	},

	orders: [{
		productId: {
				type: String
		},
			price: {
				type: Number
		},
			quantity: {
				type: Number
		},
			subtotal: {
				type: Number
		}
	}],

	totalPrice: {
		type: Number
	},

	purchasedOn: {
		type: Date,
		default: new Date()
	},

	isActive: {
		type: Boolean,
		default: true
	}	
});

module.exports = mongoose.model("Orders", orderSchema);
