const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth");

// Route for registering a user
router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for checking if the user's email already exists in the database
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for getting user details
router.get("/details", async (req, res) => {
	const user = await auth.decode(req.headers.authorization);
	userController.getProfile(user.id)
	.then(resultFromController => res.send(resultFromController)); });

// Route for registering a new admin
router.post("/admin", (req,res) => {
	userController.registerAdmin(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for authenticating a user
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for setting user as admin
router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {

	const user = auth.decode(req.headers.authorization)
	userController.updateAdmin(user, req.params, req.body).then(resultFromController => res.send(resultFromController));
})

module.exports = router;
