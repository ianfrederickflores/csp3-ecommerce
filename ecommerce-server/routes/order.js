const express = require("express");
const router = express.Router();
const orderController = require("../controllers/order");
const Order = require("../models/Orders");
const auth = require("../auth");

// Route for checkout
router.post("/checkout/:userId", auth.verify, (req, res) => {		
	const user = auth.decode(req.headers.authorization)
	let userId = req.params.userId
	let data = {
		productId: req.body.productId,
		quantity: req.body.quantity,
		price: req.body.price
	}

	orderController.addOrder(user, userId, data).then(resultFromController => res.send(resultFromController));
});

// Route to retrieve authenticated user's orders
router.get("/myOrders", auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization);
	orderController.getMyOrders(user)
	.then(resultFromController => res.send(resultFromController));
});


// Route to retrieve all orders
router.get("/", auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
	orderController.getAllOrders(user).then(resultFromController => res.send(resultFromController))
});

module.exports = router;
