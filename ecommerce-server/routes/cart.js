const express = require("express")
const jwt = require('jsonwebtoken')
const cartController = require('../controllers/cart')
const auth = require('../auth')
const router = express.Router()

// Route to retrieve all cart items
router.get("/all", (req, res, next) => {
	const user = auth.decode(req.headers.authorization);
	cartController.getAllInCart(user)
	.then(resultFromController => res.send(resultFromController))
	.catch(next)
})

// Route to add to cart
router.post("/add", async (req, res) => {
	cartController.addToCart(user, req.body)
	.then(resultFromController => res.send(resultFromController) )
})

// Route to update cart
router.put("/:cartId", (req, res) => {
	const user = auth.decode(req.headers.authorization);
	cartController.updateCart(user, req.params, req.body)
	.then(resultFromController => res.send(resultFromController));
})

// Route to remove from cart
router.put("/:cartId/:productId", (req, res) => {
	const cartId = req.params.cartId;
	const productId = req.params.productId;
	cartController.archiveProduct(cartId, productId)
	.then(resultFromController => res.send(resultFromController));
})

module.exports = router;