const express = require("express");
const router = express.Router();
const productController = require("../controllers/product");
const auth = require("../auth");

// Route to Create New Product
router.post("/", auth.verify, (req, res) => {		
	const data = auth.decode(req.headers.authorization)

	productController.addProduct(data, req.body).then(resultFromController => res.send(resultFromController));
});

// Route to retrieve a single product
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
});

// Route to retrieve all active product
router.get("/", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController))
});

// Route to update product information
router.put("/:productId", auth.verify, (req, res) => {

	const user = auth.decode(req.headers.authorization)
	productController.updateProduct(user, req.params, req.body).then(resultFromController => res.send(resultFromController));
});

// Route to archive product
router.put("/:productId/archive", auth.verify, (req, res) => {

	const user = auth.decode(req.headers.authorization)
	productController.archiveProduct(user, req.params).then(resultFromController => res.send(resultFromController));
});

module.exports = router;
